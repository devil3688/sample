import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {environment} from "../../environments/environment";
import Echo from 'laravel-echo';

declare var require: any;

@Injectable({
    providedIn: 'root'
})
export class WebsocketService {
    private websocket: WebSocket | null = null;
    private messageSubject: Subject<string> = new Subject();
    private url: string;
    private reconnectAttempts = 0;
    private maxReconnectAttempts = 5;
    private echo: Echo | null = null;
    private listeners: Set<string> = new Set();
    private heartbeatIntervalId: any = null;

    constructor() {
        this.initEcho();
        this.url = environment.websocketUrl;
    }

    private initEcho(): void {
        const io = require("socket.io-client/dist/socket.io")

        try {
            this.echo = new Echo({
                broadcaster: 'socket.io',
                host: environment.websocketUrl,  // 使用當前的 hostname
                client: io,
                options: {
                    reconnection: true,
                    reconnectionDelay: 1000,
                    reconnectionDelayMax: 5000,
                    timeout: 30000,
                    transports: ['polling']
                }
            });

            this.echo.connector.socket.on('connect', () => {
                this.reconnectAttempts = 0; // Reset reconnect attempts on successful connection
                console.log('WebSocket connected');
                this.startHeartbeat();
            });

            this.echo.connector.socket.on('disconnect', () => {
                console.log('WebSocket disconnected');
                this.handleReconnection();
                this.stopHeartbeat(); // 停止心跳包
            });

            this.echo.connector.socket.on('reconnect_attempt', () => {
                this.reconnectAttempts++;
                console.log('WebSocket reconnect attempt', this.reconnectAttempts);
                if (this.reconnectAttempts > this.maxReconnectAttempts) {
                    this.echo?.disconnect();
                    console.log('Max reconnect attempts reached');
                }
            });

        } catch (error) {
            console.error('初始化 Echo 失敗:', error);
        }
    }

    // 連接 WebSocket
    public listen(channel: string, event: string, callback: Function): void {
        const listenerKey = `${channel}:${event}`;
        if (this.echo && !this.listeners.has(listenerKey)) {
            this.echo.channel(channel).listen(event, callback);
            this.listeners.add(listenerKey);
        }
    }

    public leaveChannel(channel: string): void {
        if (this.echo) {
            this.echo.leave(channel);
            // 移除所有与该频道相关的监听器
            this.listeners.forEach(listenerKey => {
                if (listenerKey.startsWith(`${channel}:`)) {
                    this.listeners.delete(listenerKey);
                }
            });
        }
    }

    public connect(): void {
        if (this.echo && !this.isConnected()) {
            this.echo.connect();
        }
    }

    public disconnect(): void {
        if (this.echo && this.isConnected()) {
            this.echo.disconnect();
        }
    }

    public isConnected(): boolean {
        return this.echo ? this.echo.connector.socket.connected : false;
    }

    private handleReconnection(): void {
        if (this.reconnectAttempts <= this.maxReconnectAttempts) {
            setTimeout(() => {
                this.connect();
            }, 1000);
        }
    }

    private startHeartbeat(): void {
        const interval = 30000;
        this.heartbeatIntervalId = setInterval(() => {
            this.sendHeartbeat();
        }, interval);
    }

    private stopHeartbeat(): void {
        if (this.heartbeatIntervalId) {
            clearInterval(this.heartbeatIntervalId);
            this.heartbeatIntervalId = null;
        }
    }

    private sendHeartbeat(): void {
        if (this.isConnected() && this.echo) {
            this.echo.connector.socket.emit('heartbeat', {timestamp: new Date().toISOString()});
            console.log('Heartbeat sent');
        }
    }
}
