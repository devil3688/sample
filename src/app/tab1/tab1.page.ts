import {Component} from '@angular/core';
import {WebsocketService} from "../services/websocket.service";
import {Preferences} from "@capacitor/preferences";
import {forkJoin} from 'rxjs';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

    wsConnected: boolean = true;
    selectedLanguage = 'zh-TW';
    speechRate = 1;
    supportedLanguages: SpeechSynthesisVoice[] = [];
    selectedVoiceIndex: number = -1;
    selectedVoice: SpeechSynthesisVoice | null = null;
    residents: any[] = [];
    notifies: any[] = [];

    constructor(
        private websocketService: WebsocketService,
    ) {
        this.toggleWebSocketConnection();

    }

    async toggleWebSocketConnection() {
        const {value: userString} = await Preferences.get({key: 'user'});

        await Preferences.set({key: 'wsConnected', value: this.wsConnected ? 'on' : 'off'})

        if (userString) {
            const user = JSON.parse(userString);
            const agencyId = user.agency_id;
            console.log(this.wsConnected)
            console.log(this.websocketService.isConnected())

            if (this.wsConnected) {
                console.log('connecting to websocket')
                if (!this.websocketService.isConnected()) {
                    this.websocketService.connect();
                }

                this.websocketService.listen('broadcast_database_dashboard.' + agencyId, 'NotifyEvent', (data: any) => {
                    console.log('Received notify event:', data)
                    let notifyResident = this.residents.find(resident => resident.resident.id === data.notify.resident_id);
                    if (notifyResident) {
                        forkJoin({
                            bedNumberString: '1234',
                            notifyMessage: '123456'
                        }).subscribe(({bedNumberString, notifyMessage}) => {
                            const message = bedNumberString + notifyResident.bed_number + notifyResident.resident.name + notifyMessage;
                            console.log('Constructed message:', message);

                            // this.ttsService.speak(message);
                            // this.notifies.unshift({
                            //     bedNumber: notifyResident.bed_number,
                            //     residentName: notifyResident.resident.name,
                            //     message: notifyMessage,
                            //     createdAt: data.notify.created_at
                            // });
                        });
                    }
                });
            } else {
                this.websocketService.leaveChannel('broadcast_database_dashboard.' + agencyId);
                this.websocketService.disconnect();
            }
        }
    }

}
