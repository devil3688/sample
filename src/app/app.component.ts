import {Component} from '@angular/core';
import {ForegroundService} from '@capawesome-team/capacitor-android-foreground-service';
import {BatteryOptimization} from "@capawesome-team/capacitor-android-battery-optimization";
import {Capacitor} from '@capacitor/core';
import {Platform} from "@ionic/angular";
import {App} from '@capacitor/app';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    private readonly FOREGROUND_SERVICE_NOTIFICATION_ID = 19830517;

    constructor(
        private platform: Platform,
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        console.log('initializeApp started');
        try {

            const requestPermissions = await ForegroundService.requestPermissions();
            console.log(requestPermissions);
            console.log('requestPermissions completed');

            // const overlayPermissionResult = await ForegroundService.requestManageOverlayPermission();
            // console.log('requestManageOverlayPermission completed', overlayPermissionResult);

            this.requestIgnoreBatteryOptimizations();
            console.log('requestIgnoreBatteryOptimizations called');

            console.log('initializeApp');

            this.platform.ready().then(() => {
                console.log('platform ready');

                if (Capacitor.getPlatform() === 'android') {
                    console.log('Android detected');
                    App.addListener('appStateChange', async (state) => {
                        console.log('App state changed: ', state.isActive ? 'active' : 'background');

                        if (!state.isActive) {
                            console.log('App is in background, starting foreground service');
                            await this.startForegroundService();


                        } else {
                            console.log('App is in foreground, stopping foreground service');
                        }
                    });
                }
            });
        } catch (error) {
            console.error('Error in initializeApp: ', error);
        }
    }

    async startForegroundService() {
        await ForegroundService.startForegroundService({
            id: this.FOREGROUND_SERVICE_NOTIFICATION_ID,
            title: 'App is running',
            body: 'WebSocket is active',
            smallIcon: 'ic_notification', // Replace with your app's icon
        });

        console.log('Foreground service started');
    }

    async stopForegroundService() {
        await ForegroundService.stopForegroundService();
        console.log('Foreground service stopped');
    }

    requestIgnoreBatteryOptimizations() {
        BatteryOptimization.requestIgnoreBatteryOptimization().then(() => {
            console.log('Battery optimizations request sent');
        }).catch((error: any) => {
            console.log('Failed to request ignoring battery optimizations', error);
        });
    }
}
